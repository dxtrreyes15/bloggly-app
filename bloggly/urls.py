"""bloggly URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from accounts.views import (
    user_login_view, user_logout_view, user_registration_view)

urlpatterns = [
    path('', include('main.urls'), name='index'), #home/index page -- dashboard /login-required

    path('admin/', admin.site.urls),
    # accout-related url mappings
    path('login/', user_login_view, name='login'),
    path('logout/', user_logout_view, name="logout"),
    path('register/', user_registration_view, name='register'),
    
    # user profile url mapping
    path('profile/', include('user_profile.urls'), name="profile")
]

#serve media files
urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
