from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User, auth

from main.models import Following
from user_profile.models import Profile
# Create your views here.

# handle user login
def user_login_view(request):
    print('login fired', f'method is {request.method}')
    print(f'method is {request.method}')
    if request.method == 'POST': # POST request fired
        print('req method is POST')
        username = request.POST['username']
        password = request.POST['password']

        #authenticate user
        # returns a user obj is user is verified
        user = auth.authenticate(username=username, password=password)
        print(user)

        if user is None: #user isn't verified
            print('no user found')
            messages.info(request, 'Invalid Credentials.')
            return redirect('login') #<- login page
        #user is verified
        auth.login(request, user) # give login access to user
        # return redirect('/') #to home page
        return redirect('/dashboard')
    else:
        # GET Request fired
        return render(request, 'accounts/login.html')

# handle user logout
def user_logout_view(request):
    auth.logout(request) #logout a user
    return redirect('login')

# handle user registration
def user_registration_view(request):
    #get type of request
    if request.method == 'POST':
        #get post req data
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password']
        password2 = request.POST['password_conf']

        #validations
        if password1 == password2:
            if User.objects.filter(username=username).exists(): #check if username already exists in db
                #user messages feature
                messages.info(request, "Username already taken")
                #return to registration page
                return redirect('register',)
            elif User.objects.filter(email=email).exists(): # email already exists in db
                messages.info(request, "Email already taken")
                #return to registration page
                return redirect('register')
            else:
            #create new User obj
                user = User.objects.create_user(
                    username = username,
                    first_name = first_name,
                    last_name = last_name,
                    email = email,
                    password = password1)
                #save User obj
                user.save()

                # create following -> associate with the newly created user
                following = Following(user=user)
                # create user profile -> assoc with newly created user
                user_profile = Profile(user=user)

                # save to db
                following.save()
                user_profile.save()
                messages.info("User successfully created")
                return redirect('login')
        else:
            messages.info(request, "Passwords didn't match")
            return redirect('register')
    else:
        # GET Request fired
        return render(request, 'accounts/register.html')
