$(document).ready(function() {
    // disable login and register buttons on click
    $('.account_cta').on('click', function() {
        setTimeout(function(){
            $(this).prop('disabled', true)
        }, 50);
        
        setTimeout(function(){
            $(this).prop('disabled', false)
        }, 15000);
        // $(this).prop('disabled', true)
        // $(this).prop('value', "WAIT...")
    })

    // transparent placeholder on focus
    $('input').focusin(function() {
        $(this).addClass('input-transparent')
        $(this).parent().find('#input_helper').removeClass('d-none')
    })
    // return to prev state
    $('input').focusout(function() {
        $(this).removeClass('input-transparent')
        $(this).parent().find('#input_helper').addClass('d-none')
    })

    $('.btn-follow-button').on('click', function(){
        var user_id = $(this).data().user_id
        var post_id = $(this).data().post_id
        var dom = $(this)
        var status = $(this).hasClass('follow_btn')
       if(status){
        $.ajax({
            url: `/follow_user/${user_id}/`,
            type: 'POST',
            success: function(response) {
                console.log('DATA', response)
                dom.removeClass('follow_btn')
                dom.removeClass('btn-warning')

                dom.addClass('btn-secondary')
                
                dom.text('Unfollow')
                console.log('sa')
                // $(`#following_btn-${post_id}`).attr('id', ``)
            }
        })
       }
       else{
        $.ajax({
            url: `/unfollow_user/${user_id}/`,
            type: 'POST',
            success: function(response) {
                console.log('DATA DELETED: ', response)
                dom.removeClass('btn-secondary')
                
                dom.addClass('follow_btn')
                dom.addClass('btn-warning')
                
                dom.text('Follow')
                console.log('sa')
                // $(`#unfollowing_btn-${post_id}`).attr('id', 'following_btn')
            }
        })
       }
        

        
    })

    // $('.follow_btn').on('click', function() {
      
       
    // })

    // $('.unfollow_btn').on('click', function() {
    //     var user_id = $(this).data().user_id
    //     var post_id = $(this).data().post_id

        
    // })
})

