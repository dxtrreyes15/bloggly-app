from django.urls import path

from main.views import (
    main_dashboard_view,
    create_post_view,
    add_comment_view,
    follow_user_view,
    unfollow_user_view,
    index_redirect_view,
    user_follow_count_view
)

urlpatterns = [
    path('', index_redirect_view, name='index_redirect'),
    path('add_comment/<int:id>/', add_comment_view, name='comment_to_post'),
    path('create_post/', create_post_view, name='create_post'),
    path('dashboard', main_dashboard_view, name='index'),
    path('followers/', user_follow_count_view, name="user_follower_count"),
    path('follow_user/', follow_user_view, name='follow_user'),
    path('unfollow_user/<int:id>/', unfollow_user_view, name='unfollow_user')
]