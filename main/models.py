from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class BlogPost(models.Model):
    author = models.ForeignKey(User, default=None, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    img_path = models.ImageField(upload_to='uploads', blank=True)
    posted_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"User: {self.author.username}"

# Following
class Following(models.Model):
    # achieve many2many rel
    # user = models.ManyToManyField(User, default=None, null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='followers')
    following = models.ManyToManyField(User, related_name='following')

    def __str__(self):
        return f"User: {self.user.username}"
    
# Comments
class PostComment(models.Model):
    post = models.ForeignKey(BlogPost, default=None, on_delete=models.CASCADE)
    user = models.ForeignKey(User, default=None, null=True, on_delete=models.CASCADE)
    comment = models.CharField(max_length=200, blank=True)
    commented_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"PostComment: {self.post.title}"