from django import forms
from .models import (BlogPost, PostComment)

# model forms here
class BlogPostForm(forms.ModelForm):
    class Meta:
        model = BlogPost
        fields = ('title', 'description', 'img_path')
        labels = {
            'title': 'Blog Title',
            'description': 'Blog Description',
            'img_path': 'Add photo'
        }

class PostCommentForm(forms.ModelForm):
    class Meta:
        model = PostComment
        fields = ('comment',)
        labels = {
            'comment': 'Write a comment'
        }