from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import BlogPostForm, PostCommentForm
from .models import BlogPost, PostComment, Following

# Create your views here.

@login_required
def index_redirect_view(request):
    return redirect('/dashboard')

@login_required
def main_dashboard_view(request):
    #instantiate model forms
    blogpost_form = BlogPostForm()
    postcomment_form = PostCommentForm()

    # get curr user (username)
    # get curr user profile (avatar)
    # get curr user followers -> count
    # get all users -> filter (not followed yet)
    # get post comments

    # get all users
    all_users = User.objects.all()
    # get all posts
    all_posts = BlogPost.objects.all().order_by('-posted_on'); # get all filter by date -> most recent
    # get all comments
    all_comments = PostComment.objects.all().order_by('-commented_on')

    following_obj = Following.objects.get(user=request.user) # retrieve following object by curr_user
    
    users_following = following_obj.following.all() # get all users that the curr logged in user follows

    user_followers = Following.objects.filter(following__id=request.user.pk)

    # username on hover -- display follower count per user
        # get follower count -- ajax -- display via tooltip
    

    # to return
    context = {
        'forms': {'blogpost_form':blogpost_form, 'postcomment_form':postcomment_form},
        'blog_posts' : all_posts,
        'comments' : all_comments,
        'users' : all_users,
        'users_following' : users_following, # users that the curr logged user follows
        'user_follower' : user_followers, # total number of users that follows curr logged user
    }
    
    return render(request,'main/dashboard.html', context)

@login_required
def create_post_view(request):
    if request.method == "POST":
        # consume POST request data incduding FILES
        # curr = request.user.pk
        form = BlogPostForm(request.POST, request.FILES)
        if form.is_valid(): # validate
            # assign pk to field na author_id
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
        return redirect('/dashboard')

# add comment func
@login_required
def add_comment_view(request, id):
    # retrieve blogpost instance
    blog_post = BlogPost.objects.get(pk=id) 

    if request.method == "POST":
        form = PostCommentForm(request.POST) #consume
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user # who commented?
            instance.post = blog_post # on what post?
            instance.save()
        return redirect('/dashboard')

# follow func
@login_required
@csrf_exempt
def follow_user_view(request):

    # get user to follow by id
    user_to_follow = User.objects.get(pk=request.POST['user_id']) #  return user to follow
    # create  followe instance
    follow = Following.objects.get(user=request.user) # get following
    follow.following.add(user_to_follow)

    return JsonResponse({'sucess': 'success!'})

# unfollow
@login_required
@csrf_exempt
def unfollow_user_view(request, id):
    # get user to follow by id
    user_to_unfollow = User.objects.get(pk=id) #  return user to follow
    # create  followe instance
    follow = Following.objects.get(user=request.user) # get following
    follow.following.remove(user_to_unfollow)

    return JsonResponse({'sucess': 'success!'})

# get follower count by user id
@login_required
@csrf_exempt
def user_follow_count_view(request):
    follower_count = Following.objects.filter(following__id=request.GET['id']).count() # get user followers by id -> count

    return JsonResponse({'status': 'ok', 'follower_count' : follower_count}) 

    
