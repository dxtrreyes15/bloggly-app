from django.contrib import admin
from .models import (BlogPost, PostComment, Following)
# Register your models here.
admin.site.register(BlogPost)
admin.site.register(PostComment)
admin.site.register(Following)