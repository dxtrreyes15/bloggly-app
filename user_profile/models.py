from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator

# Create your models here.

# Profile
class Profile(models.Model):
    user = models.OneToOneField(User, default=None, null=True, on_delete=models.CASCADE)
    avatar = models.ImageField(default='default.jpg', upload_to='profile_images', blank=True, validators=[FileExtensionValidator(['png', 'jpg'])])
    bio = models.TextField(blank=True)
    
    def __str__(self):
        return f"User: {self.user.username}"