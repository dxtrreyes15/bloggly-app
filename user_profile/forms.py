from django import forms
from django.contrib.auth.models import User
from .models import Profile

#username udpate model form
class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username',)
        labels = {
            'username' : 'Update username'
        }

        help_texts = {
            'username': '''<ul>
                <li>Username must be unique</li>
            </ul>
            ''',
        }

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('avatar',)
        labels = {
            'avatar' : 'Select profile photo'
        }

        help_texts = {
            'avatar': '''<ul>
                <li>File types accepted: [ jpg/jpeg, png ]</li>
            </ul>
            ''',
        }