from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User

from .forms import UserUpdateForm, ProfileUpdateForm
from main.forms import PostCommentForm, BlogPostForm
from main.models import Following
from main.models import BlogPost, PostComment
from user_profile.models import Profile
from user_profile.forms import UserUpdateForm, ProfileUpdateForm

# Create your views here.

@login_required
def user_profile_view(request):
    # model fomrs here
    # set initial states
        # -get image path from Profile Obj
    user_update_form = UserUpdateForm(initial={'username': request.user.username})
    profile_update_form = ProfileUpdateForm()
    post_comment_form = PostCommentForm()
    blog_post_fomr = BlogPostForm()
    #get following, followers
    following_obj = Following.objects.get(user=request.user) # retrieve following object by curr_user
    users_following = following_obj.following.all() # get all users that the curr logged in user follows
    user_followers = Following.objects.filter(following__id=request.user.pk)

    #get profile data (icon, bio)
    profile = Profile.objects.get(user=request.user)
    print('PROFILE', profile.user)
    #get user posts
    user_posts = BlogPost.objects.filter(author__id=request.user.pk).order_by('-posted_on'); # get all filter by date -> most recent
    # get all comments
    all_comments = PostComment.objects.all().order_by('-commented_on')

    print(user_posts)
        
    context = {
        'forms' : {'user_update_form' : user_update_form, 'profile_update_form': profile_update_form, 'post_comment_form' : post_comment_form, 'blog_post_form' : blog_post_fomr },
        'following' : users_following,
        'followers' : user_followers,
        'posts' : user_posts,
        'comments' : all_comments,
        'profile' : profile
    }
    return render(request, 'profile/profile.html', context)

@login_required
def update_profile_view(request):
    print("FUNCTION INVOKED")
    # handle updates here
    if request.method == "POST":
        user_update_form = UserUpdateForm(request.POST or None, instance=request.user) # instance for updating required
        profile_update_form = ProfileUpdateForm(request.POST or None, request.FILES or None, instance=request.user.profile) # instance for updating required

        if user_update_form.is_valid() and profile_update_form.is_valid():
            print('saved')
            user_update_form.save() #save
            profile_update_form.save() #save 
            return redirect('/profile/user')
        print('not saved') # if not valid
        return redirect('/profile/user')

    user_update_form = UserUpdateForm(initial={'username': request.user.username})
    profile_update_form = ProfileUpdateForm()
    context = {
        'forms' : {'user_update_form': user_update_form, 'profile_update_form' : profile_update_form}
    }
    print('not POST METHOD')
    return render(request, 'profile/profile.html', context)
