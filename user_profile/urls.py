from django.urls import path

from .views import user_profile_view, update_profile_view

# url mappings for profile
urlpatterns = [
    path('user/', user_profile_view, name="user"),
    path('update/', update_profile_view, name='update_profile')
]